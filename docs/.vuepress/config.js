module.exports = {
  lang: 'zh-CN',
  title: 'Physics',
  description: '初中物理知识库',
  plugins: [
    [
      '@vuepress/plugin-search',
      {
        locales: {
          '/': {
            placeholder: '搜索',
          }
        },
      },
    ],
  ],
  base: '/',
  themeConfig: {
	darkMode: false,
    logo: '/images/Physics.png',
    docsRepo: 'https://bitbucket.org/Thehrz/physics',
    docsBranch: 'master',
    docsDir: 'docs',
    editLinkPattern: ':repo/src/:branch/:path?mode=edit&at=master',
    sidebar: [
      '/physics.md',
      {
        text: '声',
        link: '/sound/sound.md',
        children: [
		  '/sound/sound.md'
        ]
      },
    ],
    navbar: [
      {
        text: '重点',
        children: ['/group/foo.md', '/group/bar.md'],
      }
    ],
  }
}