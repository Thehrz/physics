---
home: true
title: 首页
heroImage: /images/Physics.png
actions:
  - text: 开始
    link: /physics.html
    type: primary
footer: MIT Licensed | Copyright © 2021-present Thehrz
---
