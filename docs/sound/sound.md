# 声的产生与传播
## 声如何传播

::: tip 定义
声音是由物体振动产生的，振动停止，发声停止。
:::
如果你尝试在说话时用手按住喉咙两侧，你会发现声带在振动，而停止说话时，则感觉不到声带的振动，这说明声音是由物体振动产生的。

::: tip 定义
正在发声的物体叫做声源。
:::
当你说话时，声带振动，所以正在发声的声带就是一个声源。